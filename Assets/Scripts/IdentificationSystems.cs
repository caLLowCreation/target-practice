﻿//InformationIntelligentSystems
namespace IdentificationSystems
{
    public enum Classification
    {
        Component,
        ModuleArray,  //Collection of Component
        System,
        Independent
    }

    public enum AmmunitionType
    {
        NotSet,
        Ballistic,
        BallisticHeavy,
    }
}
