﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace Systems.Radar
{

    [System.Serializable]
    public class RadarComponent : ScriptableObject
    {
        public float min;
        public float max;

        public float sweepSpeed;
        public float radius;
        public float current { get; set; }

        public virtual void DoOperation(string cipher, TargetInfo targetInfo) { targetInfos = null; }

        public IEnumerable<TargetInfo> targetInfos { get; set; }

        public virtual void DrawGizmos(TargetInfo targetInfo) { }
    }  
}
