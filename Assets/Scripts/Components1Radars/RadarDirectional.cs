﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;

namespace Systems.Radar
{
    [System.Serializable]
    public class RadarDirectional : RadarComponent
    {
        public override void DoOperation(string cipher, TargetInfo targetInfo)
        {
            Transform trans = targetInfo.localTransform;
            current = min + Mathf.PingPong(Time.time * sweepSpeed, max);
            targetInfos = Physics.OverlapSphere(trans.position + trans.forward * current, radius)
                .Select((x) =>
                {
                    TargetInfo ti = x.GetComponent<TargetInfo>();
                    if (ti == null)
                    {
                        ti = x.GetComponentInParent<TargetInfo>();
                    }
                    return ti;
                });
        }

        public override void DrawGizmos(TargetInfo targetInfo)
        {
            var oldColor = Gizmos.color;
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(targetInfo.transform.position + targetInfo.transform.forward * current, radius);
            Gizmos.color = oldColor;
        }
    }

}

