﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;

namespace Systems.Radar
{
    [System.Serializable]
    public class RadarSweep : RadarComponent
    {
        public float startAngle;
        public float angleRange;

        public override void DoOperation(string cipher, TargetInfo targetInfo)
        {
            Transform trans = targetInfo.localTransform;
            current = Mathf.PingPong(Time.time * sweepSpeed, angleRange) - startAngle;
            trans.rotation = targetInfo.owner.localTransform.rotation * Quaternion.Euler(Vector3.up * current);
            targetInfos = Physics.RaycastAll(trans.position + trans.forward * min, trans.forward, max)
                .Select((x) =>
                {
                    TargetInfo ti = x.collider.GetComponent<TargetInfo>();
                    if (ti == null)
                    {
                        ti = x.collider.GetComponentInParent<TargetInfo>();
                    }
                    return ti;
                });
        }

        public override void DrawGizmos(TargetInfo targetInfo)
        {
            var oldColor = Gizmos.color;
            Gizmos.color = Color.cyan;
            Gizmos.DrawRay(targetInfo.transform.position + targetInfo.transform.forward * min, targetInfo.transform.forward * max);
            Gizmos.color = oldColor;
        }
    }

}

