﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;

namespace Systems.Radar
{
    public class RadarComponents : TargetInfo
    {

        [SerializeField]
        RadarComponent[] radarComponents;

        delegate void Operation(string cipher, TargetInfo targetInfo);
        event Operation OnOperation;

        public delegate void TargetsSweep(string cipher, IEnumerable<TargetInfo> targetInfos);
        public static event TargetsSweep OnTargetsSweep;

        Operation defaultOperation = delegate(string cipher, TargetInfo targetInfo) { };

        protected override void OnEnable()
        {
            base.OnEnable();
            StartCoroutine(SweepForTargets());
        }

        IEnumerator SweepForTargets()
        {
            while (enabled)
            {
                while (ComponentsUtility.IsNullOrEmpty(radarComponents)) yield return null;

                foreach (var radarComponent in radarComponents)
                {
                    OnOperation += radarComponent ? radarComponent.DoOperation : defaultOperation;
                }

                if (OnOperation != null)
                {
                    OnOperation(iffCipher, this);
                }

                foreach (var radarComponent in radarComponents)
                {
                    OnOperation -= radarComponent ? radarComponent.DoOperation : defaultOperation;
                }

                if (OnTargetsSweep != null)
                {
                    OnTargetsSweep(iffCipher, radarComponents
                        .Where(x => x != null && x.targetInfos != null)
                        .SelectMany(x => x.targetInfos));
                }

                yield return null;
            }
        }

        void OnDrawGizmos()
        {
            if (radarComponents == null) return;
            foreach (var radarComponent in radarComponents)
            {
                if (radarComponent)
                {
                    radarComponent.DrawGizmos(this);
                }
            }
        }
    }

}

