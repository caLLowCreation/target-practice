﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace Systems.Targeting
{
    [System.Serializable]
    public class TargetingBasicSelective : TargetingBasic
    {
        [SerializeField]
        protected int takeAmount = 1;

        public override void DoOperation(string cipher, TargetInfo targetInfo, ref IEnumerable<TargetInfo> targetInfos)
        {
            m_TakeAmount = takeAmount;
            targetInfos = targetInfos
                .Where(x => x.iffCipher != cipher)
                //.Where(x => (x as Systems.Weaponize.AmmunitionInfo) == null)
                ;
            base.DoOperation(cipher, targetInfo, ref targetInfos);
        }

        public override void DrawGizmos(TargetInfo targetInfo)
        {
            if (!current) return;
            var oldColor = Gizmos.color;
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(current.transform.position, 0.75f);
            Gizmos.color = oldColor;
        }
    }
}
