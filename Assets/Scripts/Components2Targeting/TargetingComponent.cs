﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace Systems.Targeting
{
    [System.Serializable]
    public class TargetingComponent : ScriptableObject
    {

        public TargetInfo current { get; set; }

        public virtual void DoOperation(string cipher, TargetInfo targetInfo, ref IEnumerable<TargetInfo> targetInfos)
        { }

        public virtual void DrawGizmos(TargetInfo targetInfo) { }
    }
}
