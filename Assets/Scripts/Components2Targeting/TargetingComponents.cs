﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;
using Systems.Radar;

namespace Systems.Targeting
{
    public class TargetingComponents : TargetInfo
    {
        [SerializeField]
        Material targetMaterial;
        [SerializeField]
        Material backMaterial;

        [SerializeField]
        TargetingComponent[] targetingComponents;

        delegate void Operation(string cipher, TargetInfo targetInfo, ref IEnumerable<TargetInfo> targetInfos);
        event Operation OnOperation;

        public delegate void TargetsSelected(string cipher, IEnumerable<TargetInfo> targetInfos);
        public static event TargetsSelected OnTargetsSelected;

        Operation defaultOperation = delegate(string cipher, TargetInfo targetInfo, ref IEnumerable<TargetInfo> t) { };

        protected override void OnEnable()
        {
            base.OnEnable();
            
            if (ComponentsUtility.IsNullOrEmpty(targetingComponents)) return;
            foreach (var targetingComponent in targetingComponents)
            {
                OnOperation += targetingComponent ? targetingComponent.DoOperation : defaultOperation;
            }

            RadarComponents.OnTargetsSweep += OnTargetsSweep;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            RadarComponents.OnTargetsSweep -= OnTargetsSweep;
            
            if (ComponentsUtility.IsNullOrEmpty(targetingComponents)) return;
            foreach (var targetingComponent in targetingComponents)
            {
                OnOperation -= targetingComponent ? targetingComponent.DoOperation : defaultOperation;
            }
        }

        void OnTargetsSweep(string cipher, IEnumerable<TargetInfo> targetInfos)
        {
            if (iffCipher != cipher) return;
            if (ComponentsUtility.IsNullOrEmpty(targetingComponents)) return;

            if (OnOperation != null)
            {
                //Only select targeted objects with TargetInfo Attached
                targetInfos = (from targetInfo in targetInfos
                               where targetInfo
                               select targetInfo);

                OnOperation(cipher, this, ref targetInfos);

                foreach (var targetInfo in targetInfos)
                {
                    var r = targetInfo.localTransform.renderer;
                    if (r)
                    {
                        StartCoroutine(MaterialBack(r));
                        r.material = targetMaterial;
                    }
                    else
                    {
                        r = targetInfo.localTransform.GetChild(0).renderer;
                        if (r)
                        {
                            StartCoroutine(MaterialBack(r));
                            r.material = targetMaterial;
                        }
                    }
                }
            }

            if (targetInfos.Count() > 0)
            {
                if (OnTargetsSelected != null)
                {
                    OnTargetsSelected(iffCipher, targetInfos);
                }
            }
        }

        IEnumerator MaterialBack(Renderer r)
        {
            yield return new WaitForSeconds(0.2f);
            if(r) r.material = backMaterial;
        }

        void OnDrawGizmos()
        {
            if (targetingComponents == null) return;
            foreach (var targetingComponent in targetingComponents)
            {
                if (targetingComponent)
                {
                    targetingComponent.DrawGizmos(this);
                }
            }
        }
    }
}
