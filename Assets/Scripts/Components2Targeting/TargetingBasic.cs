﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace Systems.Targeting
{
    [System.Serializable]
    public class TargetingBasic : TargetingComponent
    {
        protected int m_TakeAmount = 1;

        public override void DoOperation(string cipher, TargetInfo targetInfo, ref IEnumerable<TargetInfo> targetInfos)
        {
            base.DoOperation(cipher, targetInfo, ref targetInfos);
            targetInfos = targetInfos.Take(m_TakeAmount);
            current = targetInfos.FirstOrDefault();
        }

        public override void DrawGizmos(TargetInfo targetInfo)
        {
            if (!current) return;
            var oldColor = Gizmos.color;
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(current.transform.position, 0.75f);
            Gizmos.color = oldColor;
        }
    }
}
