﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;
using Systems.Targeting;
using Systems.Weaponize.Modules;

namespace Systems.Weaponize
{
    public class WeaponizeComponents : TargetInfo
    {

        [SerializeField]
        WeaponizeModule[] weaponizeModules;

        List<TargetInfo> weaponizePool;

        bool initialized;

        IEnumerator InitializeComponents()
        {

            initialized = false;
            while (ComponentsUtility.IsNullOrEmpty(weaponizeModules)) yield return null;

            if (weaponizePool != null)
            {
                foreach (var go in weaponizePool)
                {
                    GameObject.Destroy(go.gameObject);
                }
                weaponizePool.Clear();
            }
            else
            {
                weaponizePool = new List<TargetInfo>();
            }
            
            foreach (var weaponizeModule in weaponizeModules)
            {
                weaponizeModule.InstantiatePool(weaponizePool);
            }

            var armoryName = "Weaponize Armory";
            var weaponizeArmory = GameObject.Find(armoryName);
            if (!weaponizeArmory)
            {
                weaponizeArmory = new GameObject(armoryName, typeof(TargetInfo));
            }

            var waTargetInfo = weaponizeArmory.GetComponent<TargetInfo>();
            foreach (var ammunition in weaponizePool)
            {
                ammunition.localTransform.parent = waTargetInfo.localTransform;
                ammunition.localTransform.position = waTargetInfo.localTransform.position;
                ammunition.localTransform.rotation = waTargetInfo.localTransform.rotation;
                ammunition.gameObject.SetActive(false);
            }

            initialized = weaponizeModules.Count() > 0;
            if (initialized) TargetingComponents.OnTargetsSelected += OnTargetsSelected;         
            yield return null;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            StartCoroutine(InitializeComponents());
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            TargetingComponents.OnTargetsSelected -= OnTargetsSelected;
        }

        void OnTargetsSelected(string cipher, IEnumerable<TargetInfo> targetInfos)
        {
            if (iffCipher != cipher) return;
            if (!initialized) return;

            foreach (var weaponizeModule in weaponizeModules)
            {
                weaponizeModule.FireWeapon(iffCipher, targetInfos, weaponizePool);
            }
        }

        void OnDrawGizmos()
        {
            if (!initialized) return;
            foreach (var weaponizeModule in weaponizeModules)
            {
                if (weaponizeModule)
                {
                    weaponizeModule.DrawGizmos(this);
                }
            }
        }
    }
}
