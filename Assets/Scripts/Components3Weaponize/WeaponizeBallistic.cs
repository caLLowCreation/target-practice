﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Systems.Weaponize
{
    [System.Serializable]
    public class WeaponizeBallistic : WeaponizeComponent
    {
        [SerializeField]
        float m_Force = 10f;

        public override IEnumerator FireAmmunition(AmmunitionInfo ammunition, Transform spawnPoint, IEnumerable<TargetInfo> targetInfos)
        {
            if (!ammunition) yield break;
            ammunition.gameObject.SetActive(true);
            ammunition.localTransform.position = spawnPoint.position;
            ammunition.localTransform.rotation = spawnPoint.rotation;
            ammunition.rigidbody.velocity = spawnPoint.forward * m_Force;
        }
    }
}
