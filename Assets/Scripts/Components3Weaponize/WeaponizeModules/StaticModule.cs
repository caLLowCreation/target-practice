﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Systems.Weaponize.Modules
{
    public class StaticModule : WeaponizeModule
    {

        protected override IEnumerator EnterSetup()
        {
            currentState = ActiveStates.Dormant;
            yield return null;
        }
    }
}
