﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Systems.Weaponize.Modules
{
    public class ActiveModule : WeaponizeModule
    {
        [Space(2)]
        [Header("ActiveModule")]
        [SerializeField]
        Transform m_PivotPoint;
        [SerializeField]
        float m_AimSpeed = 1.0f;
        [SerializeField]
        float m_AimMargin = 5.0f;
        [SerializeField]
        float m_AimAngleLimit = 45;
        [SerializeField]
        float m_StayAlertTime = 1;

        [Header("Status")]
        [SerializeField]
        TargetInfo m_NextTarget;
        [SerializeField]
        float m_TrackingAngle;
        [SerializeField]
        int m_FiredCount;
        [SerializeField]
        float m_StayAlertTimer;

        Quaternion m_StartRotation;

        protected override IEnumerator EnterSetup()
        {
            m_StartRotation = m_PivotPoint.rotation; 
            currentState = ActiveStates.Dormant;
            yield return null;
        }

        protected override IEnumerator EnterDormant()
        {
            m_NextTarget = null;
            yield return base.EnterDormant();
        }

        protected override IEnumerator EnterFiring()
        {
            ++m_FiredCount;
            yield return base.EnterFiring();
        }

        protected override IEnumerator EnterAlerted()
        {
            m_StayAlertTimer = 0;
            yield return base.EnterAlerted();
        }

        protected override IEnumerator StayDormant()
        {
            m_PivotPoint.rotation = Quaternion.Lerp(m_PivotPoint.rotation, m_StartRotation, Time.deltaTime * m_AimSpeed);
            yield return base.StayDormant();
        }

        protected override IEnumerator StayTargeting()
        {
            var newRotation = Quaternion.LookRotation(m_NextTarget.localTransform.position - m_PivotPoint.position);
            var targetRotation = m_PivotPoint.rotation;

            if (Quaternion.Angle(newRotation, m_StartRotation) <= m_AimAngleLimit)
                targetRotation = newRotation;

            m_PivotPoint.rotation = Quaternion.Lerp(m_PivotPoint.rotation, targetRotation, Time.deltaTime * m_AimSpeed);
            m_TrackingAngle = Quaternion.Angle(targetRotation, m_PivotPoint.rotation);

            //if aimed at target
            if (m_TrackingAngle < m_AimMargin) currentState = ActiveStates.Firing;
            yield return base.StayTargeting();
        }

        protected override IEnumerator StayFiring()
        {
            yield return StartCoroutine(FireLoadedAmmunitionAtTargetsInRanged(new TargetInfo[] { m_NextTarget }));
            currentState = ActiveStates.Alerted;
            yield return base.StayFiring();
        }

        protected override IEnumerator StayAlerted()
        {
            m_StayAlertTimer += Time.deltaTime;
            if (m_StayAlertTimer >= m_StayAlertTime) currentState = ActiveStates.Dormant;
            yield return base.StayAlerted();
        }

        protected override IEnumerator FireLoadedAmmunition(IEnumerable<TargetInfo> targetInfos)
        {
            if (!m_PivotPoint) yield break;
            m_NextTarget = targetInfos.FirstOrDefault();
            currentState = m_NextTarget ? ActiveStates.Targeting : ActiveStates.Dormant;
        }

    }
}
