﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Systems.Weaponize.Modules
{
    public abstract class WeaponizeModule : TargetInfo
    {
        public enum ActiveStates
        {
            Setup,
            Dormant,
            Targeting,
            Firing,
            Alerted
        }

        public struct StateActions
        {
            public Func<IEnumerator> enter;
            public Func<IEnumerator> stay;
            public Func<IEnumerator> exit;

            public StateActions(Func<IEnumerator> enter, Func<IEnumerator> stay, Func<IEnumerator> exit)
            {
                this.enter = enter;
                this.stay = stay;
                this.exit = exit;
            }
        }

        [Space(2)]
        [Header("WeaponizeModule")]
        public WeaponizeComponent weaponizeComponent;
        [SerializeField]
        Transform m_SpawnPoint;
        [SerializeField]
        float m_EffectiveRange = 5f;
        [SerializeField]
        int m_Capacity;
        [SerializeField]
        float m_ReloadTime = 1f;

        AmmunitionInfo m_LoadedAmmunition;
        Dictionary<ActiveStates, StateActions> stateAction;
        
        [Header("Status")]
        [SerializeField]
        ActiveStates m_CurrentState;
        [SerializeField]
        float m_ReloadTimer;

        public ActiveStates currentState
        {
            get { return m_CurrentState; }
            protected set
            {
                StartCoroutine(stateAction[m_CurrentState].exit());
                m_CurrentState = value;
                StartCoroutine(stateAction[m_CurrentState].enter());
            }
        }

        public void InstantiatePool(List<TargetInfo> weaponizePool)
        {
            for (int i = 0; i < m_Capacity; i++)
            {
                var ammunition = weaponizeComponent.InstantiateAmmunition();
                CreateCipher.UpdateCiphers(ammunition, iffCipher);
                var weapon = ammunition.GetComponent<TargetInfo>(); 
                weaponizePool.Add(weapon);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            stateAction = new Dictionary<ActiveStates, StateActions>(5);
            stateAction.Add(ActiveStates.Setup, new StateActions(EnterSetup, StaySetup, ExitSetup));
            stateAction.Add(ActiveStates.Dormant, new StateActions(EnterDormant, StayDormant, ExitDormant));
            stateAction.Add(ActiveStates.Targeting, new StateActions(EnterTargeting, StayTargeting, ExitTargeting));
            stateAction.Add(ActiveStates.Firing, new StateActions(EnterFiring, StayFiring, ExitFiring));
            stateAction.Add(ActiveStates.Alerted, new StateActions(EnterAlerted, StayAlerted, ExitAlerted));
        }

        protected abstract IEnumerator EnterSetup();
        protected virtual IEnumerator EnterDormant() { yield return null; }
        protected virtual IEnumerator EnterTargeting() { yield return null; }
        protected virtual IEnumerator EnterFiring() { yield return null; }
        protected virtual IEnumerator EnterAlerted() { yield return null; }

        protected virtual IEnumerator StaySetup() { yield return null; }
        protected virtual IEnumerator StayDormant() { yield return null; }
        protected virtual IEnumerator StayTargeting() { yield return null; }
        protected virtual IEnumerator StayFiring() { yield return null; }      
        protected virtual IEnumerator StayAlerted() { yield return null; }

        protected virtual IEnumerator ExitSetup() { yield return null; }
        protected virtual IEnumerator ExitDormant() { yield return null; }
        protected virtual IEnumerator ExitTargeting() { yield return null; }
        protected virtual IEnumerator ExitFiring() { yield return null; }
        protected virtual IEnumerator ExitAlerted() { yield return null; }

        IEnumerator StartModule()
        {
            currentState = ActiveStates.Setup;

            while (enabled) yield return StartCoroutine(stateAction[m_CurrentState].stay());
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            StartCoroutine(StartModule());
        }

        public void FireWeapon(string cipher, IEnumerable<TargetInfo> targetInfos, IEnumerable<TargetInfo> weaponizePool)
        {
            if (iffCipher == cipher && Reloaded(weaponizePool))
            {
                StartCoroutine(FireLoadedAmmunition(targetInfos));
            }
        }

        protected virtual IEnumerator FireLoadedAmmunition(IEnumerable<TargetInfo> targetInfos)
        {
            yield return StartCoroutine(FireLoadedAmmunitionAtTargetsInRanged(targetInfos));
        }

        protected IEnumerator FireLoadedAmmunitionAtTargetsInRanged(IEnumerable<TargetInfo> targetInfos)
        {
            var targetsInRange = targetInfos
                .Where(x => Vector3.Distance(x.localTransform.position, m_SpawnPoint.position) <= m_EffectiveRange);
            if (targetsInRange.Count() > 0)
            {
                yield return StartCoroutine(weaponizeComponent.FireAmmunition(m_LoadedAmmunition, m_SpawnPoint, targetInfos));
            }
        }

        bool Reloaded(IEnumerable<TargetInfo> weaponizePool)
        {
            if (!reloadTimerReady) return false;
            m_ReloadTimer = Time.timeSinceLevelLoad + m_ReloadTime;
            //pick weapon from pool
            m_LoadedAmmunition = (from weapon in weaponizePool
                              where
                        ((AmmunitionInfo)weapon).ammunitionType == weaponizeComponent.ammunitionType
                        && !weapon.localTransform.gameObject.activeSelf
                              select weapon as AmmunitionInfo).FirstOrDefault();
            return m_LoadedAmmunition;
        }

        bool reloadTimerReady 
        { 
            get 
            { 
                return m_ReloadTimer < Time.timeSinceLevelLoad; 
            } 
        }

        public virtual void DrawGizmos(TargetInfo targetInfo)
        {
            if (!weaponizeComponent) return;
            weaponizeComponent.DrawGizmos(targetInfo);
            if (!m_SpawnPoint) return;
            var oldColor = Gizmos.color;
            Gizmos.color = Color.Lerp(Color.magenta, Color.yellow, 0.8f);
            Gizmos.DrawRay(m_SpawnPoint.position, m_SpawnPoint.forward * m_EffectiveRange);
            Gizmos.color = oldColor;
        }

    }
}
