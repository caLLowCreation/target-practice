﻿using UnityEngine;
using IdentificationSystems;

namespace Systems.Weaponize
{
    public class AmmunitionInfo : TargetInfo
    {
        [SerializeField]
        AmmunitionType m_AmmunitionType;

        public AmmunitionType ammunitionType { get { return m_AmmunitionType; } }
    }

}
