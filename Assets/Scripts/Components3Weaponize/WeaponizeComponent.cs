﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using IdentificationSystems;
using System.Collections;

namespace Systems.Weaponize
{
    [System.Serializable]
    public class WeaponizeComponent : ScriptableObject
    {
        [SerializeField]
        GameObject m_AmmunitionPrefab;
        int m_AmmunitionType = -1;

        public virtual void DrawGizmos(TargetInfo targetInfo) { }

        public virtual GameObject InstantiateAmmunition()
        {
            return Instantiate(m_AmmunitionPrefab) as GameObject;
        }

        public AmmunitionType ammunitionType
        {
            get
            {
                if (m_AmmunitionType == -1)
                {
                    m_AmmunitionType = (int)m_AmmunitionPrefab.GetComponent<AmmunitionInfo>().ammunitionType;
                }
                return (AmmunitionType)m_AmmunitionType;
            }
        }

        public virtual IEnumerator FireAmmunition(AmmunitionInfo ammunition, Transform spawnPoint, IEnumerable<TargetInfo> targetInfos)
        {
            if (!ammunition) yield break;
            ammunition.gameObject.SetActive(true);
            ammunition.localTransform.position = spawnPoint.position;
            ammunition.localTransform.rotation = spawnPoint.rotation;
        }
    }
}
