﻿using Systems.Radar;
using Systems.Targeting;
using Systems.Weaponize;
using UnityEditor;
using UnityEngine;

/// <summary>
//	This makes it easy to create, name and place unique new YourClassAsset asset files.
// http://wiki.unity3d.com/index.php?title=CreateScriptableObjectAsset
/// </summary>
public static class ProjectMenuItems //YourClassAsset
{

    [MenuItem("Assets/Create/Systems/Radar/Default")]
    public static void CreateAssetRadarComponent()
    {
        ScriptableObjectUtility.CreateAsset<RadarComponent>();
    }

    [MenuItem("Assets/Create/Systems/Radar/Directional")]
    public static void CreateAssetRadarDirectional()
    {
        ScriptableObjectUtility.CreateAsset<RadarDirectional>();
    }

    [MenuItem("Assets/Create/Systems/Radar/Sweep")]
    public static void CreateAssetRadarSweep()
    {
        ScriptableObjectUtility.CreateAsset<RadarSweep>();
    }

    [MenuItem("Assets/Create/Systems/Targeting/Default")]
    public static void CreateAssetTargetingComponent()
    {
        ScriptableObjectUtility.CreateAsset<TargetingComponent>();
    }

    [MenuItem("Assets/Create/Systems/Targeting/Basic")]
    public static void CreateAssetTargetingBasic()
    {
        ScriptableObjectUtility.CreateAsset<TargetingBasic>();
    }

    [MenuItem("Assets/Create/Systems/Targeting/Basic Selective")]
    public static void CreateAssetTargetingBasicSelective()
    {
        ScriptableObjectUtility.CreateAsset<TargetingBasicSelective>();
    }

    [MenuItem("Assets/Create/Systems/Weaponize/Default")]
    public static void CreateAssetWeaponizeComponent()
    {
        ScriptableObjectUtility.CreateAsset<WeaponizeComponent>();
    }

    [MenuItem("Assets/Create/Systems/Weaponize/Ballistic")]
    public static void CreateAssetWeaponizeBallistic()
    {
        ScriptableObjectUtility.CreateAsset<WeaponizeBallistic>();
    }

}
