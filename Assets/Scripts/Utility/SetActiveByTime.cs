﻿using UnityEngine;
using System.Collections;

public class SetActiveByTime : MonoBehaviour
{
    public bool activate;
    public float time;

    public void OnEnable()
    {
        Invoke("SetState", time);
    }

    void SetState()
    {
        gameObject.SetActive(activate);
    }
}
