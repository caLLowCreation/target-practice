﻿using UnityEngine;

public static class CreateCipher
{
    public static void UpdateCiphers(GameObject gameObject, string cipher)
    {
        gameObject.GetComponent<TargetInfo>().SetCipher(cipher);
        var targetInfos = gameObject.GetComponentsInChildren<TargetInfo>();
        foreach (var targetInfo in targetInfos)
        {
            targetInfo.SetCipher(cipher);
        }
    }
}
