﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class ComponentsUtility
{

    public static bool IsNullOrEmpty<T>(IEnumerable<T> components)
    {
        return components == null || components.Count() == 0;
    }

}
