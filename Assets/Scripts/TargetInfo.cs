﻿using UnityEngine;
using System.Collections;
using IdentificationSystems;

public class TargetInfo : MonoBehaviour
{
    [SerializeField]
    bool createCipher;
    [SerializeField]
    string m_IffCipher;

    Transform m_LocalTransform;
    TargetInfo m_Owner;

    protected virtual void Awake() { }

	protected virtual void OnEnable() 
    {
        if (createCipher)
        {
            var cipher = System.Guid.NewGuid().ToString();
            CreateCipher.UpdateCiphers(gameObject, cipher);
        }
	}

    protected virtual void OnDisable() { }

    public Transform localTransform 
    { 
        get 
        {
            if (!m_LocalTransform) m_LocalTransform = GetComponent<Transform>();
            return m_LocalTransform; 
        } 
    }
    
    public TargetInfo owner 
    { 
        get 
        {
            if (!m_Owner) m_Owner = localTransform.root.GetComponent<TargetInfo>();
            if (!m_Owner) m_Owner = GetComponent<TargetInfo>();
            return m_Owner; 
        } 
    }

    public string iffCipher { get { return m_IffCipher; } }

    internal void SetCipher(string iffCipher)
    {
        m_IffCipher = iffCipher;
    }
}